﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Imaging;
using System.IO;
using System.Threading.Tasks;
using MandlebrotTest.Algorithms;
using TransferFunctions;
using UIControls;
using ArbitraryPrecision;

namespace MandlebrotTest
{
    public partial class RenderForm : Form
    {
        List<IEscapeTimeFractal> algorithms = new List<IEscapeTimeFractal>();
        List<TransferFunctions.ITransferFunction> transferFunctions = new List<ITransferFunction>();

        IEscapeTimeFractal selectedAlgorithm = null;

        float zoom = 1;

        const int MAX_ITERATIONS = 10;

        Bitmap bitmap;

        public RenderForm()
        {
            InitializeComponent();

            loadAlgorithms();
            loadTransferFunctions();   
        }

        private void loadAlgorithms()
        {
            //  Load the algorithms
            algorithms.Add(new MandlebrotAlgorithm());
            //algorithms.Add(new JuliaAlgorithm());
            //algorithms.Add(new BurningShipAlgorithm());

            //  Fill the algorithm combobox
            for (int i = 0; i < algorithms.Count; ++i)
            {
                comboBoxAlgorithm.Items.Add(algorithms[i]);
            }

            comboBoxAlgorithm.SelectedIndex = 0;
        }

        private void loadTransferFunctions()
        {
            transferFunctions.Add(new OpaqueTransferFunction(Color.Red, Color.Red));
            transferFunctions.Add(new OpaqueTransferFunction(Color.White, Color.MidnightBlue));
            transferFunctions.Add(new OpaqueTransferFunction(Color.MidnightBlue, Color.White));
            transferFunctions.Add(new OpaqueTransferFunction(Color.Yellow, Color.Green));
            transferFunctions.Add(new OpaqueTransferFunction(Color.Green, Color.Yellow));
            transferFunctions.Add(new OpaqueTransferFunction(TransferFunction.ColorFromHSV(0,0.1,1),TransferFunction.ColorFromHSV(255,0.1,1),"Rainbow spectrum"));
            
            fillTransferFunctions();
        }

        private void fillTransferFunctions()
        {
            comboBoxTransferFunction.Items.Clear();

            foreach (ITransferFunction tff in transferFunctions)
            {
                comboBoxTransferFunction.Items.Add(tff);
            }

            if (comboBoxTransferFunction.Items.Count > 0)
            {
                comboBoxTransferFunction.SelectedIndex = 0;
            }
        }

        private void createBitmap(int width, int height)
        {
            const int BPP = 3;

            bitmap = new Bitmap(width, height);

            BitmapData bytes = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height),ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
            IntPtr hBitmap = bytes.Scan0;

            ITransferFunction selectedTransferFunction = (ITransferFunction)comboBoxTransferFunction.SelectedItem;

            unsafe
            {
                byte* pointer = (byte*)hBitmap;
#if PARALLEL
                Parallel.For(0, height, new ParallelOptions { MaxDegreeOfParallelism = 8 }, y =>
#else
                for (int y = 0; y < height; ++y)
#endif
                {
                    for (int x = 0; x < width; ++x)
                    {
                        //  TODO: find min/max within a data set for normalization
                        Color pixelColour = Color.Black;

                        int pixelIndex = BPP * ((y * width) + x);

                        if (selectedAlgorithm.CountToInfinity[x, y] > 0)
                        {
                            double normalisedValue;

                            //pixelColour = Color.Yellow;
                            if (checkBoxFixedBounds.Checked)
                            {
                                normalisedValue =
                                    TransferFunction.NormalizeValue(0, MAX_ITERATIONS, selectedAlgorithm.CountToInfinity[x, y]);
                            }
                            else
                            {
                                normalisedValue =
                                    TransferFunction.NormalizeValue(selectedAlgorithm.MinValue, selectedAlgorithm.MaxValue, selectedAlgorithm.CountToInfinity[x, y]);
                            }
                            pixelColour = selectedTransferFunction.ColorForValue(normalisedValue);
                        }
                        
                        //  Actually it is BGR encoding not RGB
                        pointer[pixelIndex + 0] = pixelColour.B;
                        pointer[pixelIndex + 1] = pixelColour.G;
                        pointer[pixelIndex + 2] = pixelColour.R;
                    }
#if PARALLEL
                });
#else
                }
#endif
            }

            bitmap.UnlockBits(bytes);
        }

        private void saveToFile(string filename)
        {
            if ((filename != "") && (bitmap != null))
            {
                string extension = Path.GetExtension(filename);

                if (extension == ".png")
                    bitmap.Save(filename, ImageFormat.Png);
                else if (extension == ".bmp")
                    bitmap.Save(filename, ImageFormat.Bmp);
                else if (extension == ".jpg")
                    bitmap.Save(filename, ImageFormat.Jpeg);
            }
        }

        private void fillPresets()
        {
            comboBoxPreset.Items.Clear();

            foreach (Preset p in selectedAlgorithm.Presets)
            {
                comboBoxPreset.Items.Add(p);
            }

            if (comboBoxPreset.Items.Count > 0)
            {
                comboBoxPreset.SelectedIndex = 0;
            }
        }

        private void comboBoxAlgorithm_SelectedIndexChanged(object sender, EventArgs e)
        {            
            selectedAlgorithm = (IEscapeTimeFractal)comboBoxAlgorithm.SelectedItem;
            fillPresets();
        }

        private void generateValues()
        {
            int width = (int)numericUpDownWidth.Value;
            int height = (int)numericUpDownHeight.Value;
            
            Preset preset = (Preset)comboBoxPreset.SelectedItem;

            selectedAlgorithm.Origin = new BigDecimalComplex(preset.origin.Real, preset.origin.Imag);
            selectedAlgorithm.C = new BigDecimalComplex(preset.constant.Real, preset.constant.Imag);

            DateTime generateStart = DateTime.Now;
            selectedAlgorithm.Generate(preset.Zoom, MAX_ITERATIONS, width, height);
            Console.WriteLine("Generated in {0} ms", (DateTime.Now - generateStart).Milliseconds);

            DateTime bitmapStart = DateTime.Now;
            createBitmap(width, height);
            Console.WriteLine("Convert to bitmap in {0} ms", (DateTime.Now - bitmapStart).Milliseconds);

            doubleBufferedPanel.Invalidate();

            if (checkBoxSaveToFile.Checked)
            {
                String dateString = DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString();
                String timeString = DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString();
                String filename = selectedAlgorithm.ToString() + "_" + preset.ToString() + "_" + dateString + "_" + timeString + ".png";
                saveToFile(filename);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            generateValues();
        }

        private PointF calculateOffset()
        {
            //  Calculate top left coordinate from the screen and bitmap centres
            float x = (doubleBufferedPanel.Width / 2) - (bitmap.Width / 2);
            float y = (doubleBufferedPanel.Height / 2) - (bitmap.Height / 2);

            return new PointF(x, y);
        }

        private void drawingPanel_Paint(object sender, PaintEventArgs e)
        {
            base.OnPaint(e);

            if ((bitmap != null) && (selectedAlgorithm.Initialized != false))
            {
                e.Graphics.DrawImage(bitmap, calculateOffset());
            }
        }

        private void drawingPanel_MouseMove(object sender, MouseEventArgs e)
        {
            if ((selectedAlgorithm.ComplexNumbers != null) && (selectedAlgorithm != null))
            {
                Point mousePos = mouseCorrectedToScreenSpace(e.Location);

                if (((mousePos.X > 0) && (mousePos.Y > 0)) &&
                    ((mousePos.X < selectedAlgorithm.Width) && (mousePos.Y < selectedAlgorithm.Height)))
                    labelPosition.Text = "[" + selectedAlgorithm.ComplexNumbers[mousePos.X, mousePos.Y].Real.ToString() + " + " + selectedAlgorithm.ComplexNumbers[mousePos.X, mousePos.Y].Imag.ToString() + "i]";
            }
        }

        private Point mouseCorrectedToScreenSpace(Point mousePos)
        {
            PointF offset = calculateOffset();

            int pixelX = mousePos.X - (Int16)offset.X;
            int pixelY = mousePos.Y - (Int16)offset.Y;

            return new Point(pixelX, pixelY);
        }

        private void comboBoxPreset_SelectedValueChanged(object sender, EventArgs e)
        {
            zoom = ((Preset)comboBoxPreset.Items[comboBoxPreset.SelectedIndex]).Zoom;
        }

        private void drawingPanel_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
                zoom *= 2;
            else if (e.Button == MouseButtons.Right)
                zoom /= 2;

            labelZoom.Text = zoom.ToString();

            if (selectedAlgorithm != null)
            {
                if (selectedAlgorithm.ComplexNumbers != null)
                {
                    Point mousePos = mouseCorrectedToScreenSpace(e.Location);

                    if (((mousePos.X > 0) && (mousePos.Y > 0)) &&
                    ((mousePos.X < selectedAlgorithm.Width) && (mousePos.Y < selectedAlgorithm.Height)))
                    {
                        selectedAlgorithm.Origin.Real = selectedAlgorithm.ComplexNumbers[mousePos.X, mousePos.Y].Real;
                        selectedAlgorithm.Origin.Imag = selectedAlgorithm.ComplexNumbers[mousePos.X, mousePos.Y].Imag;

                        int width = (int)numericUpDownWidth.Value;
                        int height = (int)numericUpDownHeight.Value;


                        //  Recalculate
                        DateTime generateStart = DateTime.Now;
                        selectedAlgorithm.Generate(zoom, MAX_ITERATIONS, width, height);
                        Console.WriteLine("Generated in {0} ms", (DateTime.Now - generateStart).Milliseconds);

                        DateTime bitmapStart = DateTime.Now;
                        createBitmap(width, height);
                        Console.WriteLine("Convert to bitmap in {0} ms", (DateTime.Now - bitmapStart).Milliseconds);

                        doubleBufferedPanel.Invalidate();
                    }
                }
            }
        }

        private void comboBoxTransferFunction_SelectedIndexChanged(object sender, EventArgs e)
        {
            ITransferFunction selectedTransferFunction = (ITransferFunction)comboBoxTransferFunction.SelectedItem;

            colourSpectrumPanel.TransferFunction = selectedTransferFunction;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            BigDecimal x = 144;
            Console.WriteLine("{0} {1}", x, BigDecimal.Sqrt(x).ToString(true));
        }

    }
}
