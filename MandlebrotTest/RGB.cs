﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MandlebrotTest
{
    class RGB
    {
        private byte r;
        private byte g;
        private byte b;

        public Byte R { get { return r; } set { r = value;} }
        public Byte G { get { return g; } set { g = value; } }
        public Byte B { get { return b; } set { b = value; } }

        public RGB(Byte r, Byte g, Byte b)
        {
            this.r = r;
            this.b = b;
            this.g = g;
        }
    }
}
