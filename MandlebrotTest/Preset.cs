﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LinearAlgebra;

namespace MandlebrotTest
{
    public class Preset
    {
        public string name;
        public Complex origin;
        
        private float zoom;
        public float Zoom { get { return zoom; } }
        
        public Complex constant;


        public Preset(string name, Complex origin, float zoom, Complex constant)
        {
            this.name = name;
            this.origin = origin;
            this.zoom = zoom;
            this.constant = constant;
        }

        public override string ToString()
        {
            return name;
        }
    }
}
