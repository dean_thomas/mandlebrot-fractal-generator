﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Drawing;

namespace MandlebrotTest
{
    public class MandlebrotCalculator
    {
        private BackgroundWorker bw = new BackgroundWorker();

        public event EventHandler CalculationProgress;
        public event EventHandler CalculationCompleted;

        public Complex[,] complexNo;
        public int[,] countToInfinity;

        Size dimensions;
        Complex origin;
        double zoom;
        int iterations;

        public MandlebrotCalculator()
        {
            bw.WorkerReportsProgress = true;
            bw.WorkerSupportsCancellation = true;
            bw.DoWork += new DoWorkEventHandler(bw_DoWork);
            bw.ProgressChanged += new ProgressChangedEventHandler(bw_ProgressChanged);
            bw.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bw_RunWorkerCompleted);


        }

        public void Calculate(Size dimensions, Complex origin, double zoom, int iterations)
        {
            this.dimensions = dimensions;
            this.origin = origin;
            this.zoom = zoom;
            this.iterations = iterations;

            bw.RunWorkerAsync();
        }

        public void Stop()
        {
            bw.CancelAsync();
        }

        void bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            EventHandler handler = CalculationCompleted;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        void bw_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            EventHandler handler = CalculationProgress;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        void bw_DoWork(object sender, DoWorkEventArgs e)
        {
            /*
             * /*
            for (int i = 1; (i <= 10); i++)
            {
                if ((worker.CancellationPending == true))
                {
                    e.Cancel = true;
                    break;
                }
                else
                {*/

            BackgroundWorker worker = sender as BackgroundWorker;

            Complex z;
            Complex c;

            complexNo = new Complex[dimensions.Width, dimensions.Height];
            countToInfinity = new int[dimensions.Width, dimensions.Height];

            //  Start with a range of 2 in each direction from zero
            //  as the zoom increases, this decreses to give more detail
            double r = 2.0d / zoom;

            double LOWER_LIMIT_X = origin.Real - r;
            double UPPER_LIMIT_X = origin.Real + r;
            double RANGE_X = (UPPER_LIMIT_X - LOWER_LIMIT_X);// / zoom;

            double LOWER_LIMIT_Y = origin.Imag - r;
            double UPPER_LIMIT_Y = origin.Imag + r;
            double RANGE_Y = (UPPER_LIMIT_Y - LOWER_LIMIT_Y);// / zoom;



            double step_x = (RANGE_X / (double)dimensions.Width);
            double step_y = (RANGE_Y / (double)dimensions.Height);

            double progress = 0;
            double progress_per_loop = (double)100 / (double)(dimensions.Height * dimensions.Width);

            for (int coordX = 0; coordX < dimensions.Width; coordX++)
            {
                double x = LOWER_LIMIT_X + (coordX * step_x);

                for (int coordY = 0; coordY < dimensions.Height; coordY++)
                {
                    double y = LOWER_LIMIT_Y + (coordY * step_y);

                    complexNo[coordX, coordY] = new Complex(x, y);

                    c = new Complex(x, y);
                    z = c;

                    int i;
                    int iterationWhereInfinte = 0;

                    for (i = 0; i < iterations; i++)
                    {
                        z = z.Squared + c;

                        if (z.IsInfinite)
                        {
                            iterationWhereInfinte = i;
                            break;
                        }
                    }

                    countToInfinity[coordX, coordY] = iterationWhereInfinte;

                    progress += progress_per_loop;
                    worker.ReportProgress((int)progress);
                    //Console.WriteLine("{0},{1} = {2}", x, y, z.IsInfinite);
                }
            }

            // Perform a time consuming operation and report progress.
            //System.Threading.Thread.Sleep(500);
            //
        }
    }
}
