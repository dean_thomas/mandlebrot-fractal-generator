﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LinearAlgebra;

namespace MandlebrotTest.Algorithms
{
    class JuliaAlgorithm : IEscapeTimeFractal
    {
        Complex[,] complexNo;
        int[,] countToInfinity;
        public Complex[,] ComplexNumbers { get { return complexNo; } }
        public int[,] CountToInfinity { get { return countToInfinity; } }

        Complex origin;
        Complex c;
        public Complex Origin { get { return origin; } set { origin = value; } }
        public Complex C { get { return c; } set { c = value; } }

        List<Preset> presets = new List<Preset>();
        public List<Preset> Presets { get { return presets; } }

        int width;
        public int Width { get { return width; } }
        int height;
        public int Height { get { return height; } }

        int minValue = 0;
        int maxValue = 0;

        Boolean initialized;
        public Boolean Initialized { get { return initialized; } }

        public JuliaAlgorithm()
        {
            initialized = false;

            LoadPresets();
        }

        public void LoadPresets()
        {
            presets.Add(new Preset("Default", new Complex(0, 0), 1, new Complex(0.32, 0.043)));
            presets.Add(new Preset("Default2", new Complex(0, 0), 1, new Complex(-0.8, 0.156)));
            presets.Add(new Preset("Default3", new Complex(0, 0), 1, new Complex(-0.70176, -0.3842)));
        }

        /**
         *  Create a Julia fractal
         **/
        public void Generate(double zoom, int iterations, int width, int height)
        {
            complexNo = new Complex[width, height];
            countToInfinity = new int[width, height];

            this.width = width;
            this.height = height;

            //  Start with a range of 2 in each direction from zero
            //  as the zoom increases, this decreses to give more detail
            double r = 2.0d / zoom;

            double LOWER_LIMIT_X = origin.Real - r;
            double UPPER_LIMIT_X = origin.Real + r;
            double RANGE_X = (UPPER_LIMIT_X - LOWER_LIMIT_X);// / zoom;

            double LOWER_LIMIT_Y = origin.Imag - r;
            double UPPER_LIMIT_Y = origin.Imag + r;
            double RANGE_Y = (UPPER_LIMIT_Y - LOWER_LIMIT_Y);// / zoom;

            double step_x = (RANGE_X / width);
            double step_y = (RANGE_Y / height);

#if PARALLEL
            Parallel.For(0, width, new ParallelOptions { MaxDegreeOfParallelism = 8 }, coordX =>
#else
            for (int coordX = 0; coordX < width; coordX++)
#endif
            {
                double x = LOWER_LIMIT_X + (coordX * step_x);

                for (int coordY = 0; coordY < height; coordY++)
                {
                    double y = LOWER_LIMIT_Y + (coordY * step_y);

                    complexNo[coordX, coordY] = new Complex(x, y);

                    //  Difference between Mandlebrot and
                    //  Julia set is that in Julia set the
                    //  multiplication constant is specified
                    //  whereas in a Mandlebrot fractal it
                    //  is fixed at the origin (z)
                    Complex zN = new Complex(x, y);
                    //Complex c = c;

                    int i;
                    int iterationWhereInfinte = 0;

                    for (i = 0; i < iterations; i++)
                    {
                        zN = zN.Squared + c;

                        if (zN.IsInfinite)
                        {
                            iterationWhereInfinte = i;
                            break;
                        }
                    }

                    countToInfinity[coordX, coordY] = iterationWhereInfinte;
                    //Console.WriteLine("{0},{1} = {2}", x, y, z.IsInfinite);
                }
#if PARALLEL
            });
#else
            }
#endif

            findMinAndMaxValues();

            initialized = true;
        }

        override public String ToString()
        {
            return "Julia set";
        }


        public int MinValue
        {
            get { return minValue; }
        }

        public int MaxValue
        {
            get { return maxValue; }
        }

        private void findMinAndMaxValues()
        {
            maxValue = 0;
            minValue = 255;

            for (int coordX = 0; coordX < width; coordX++)
            {
                for (int coordY = 0; coordY < height; coordY++)
                {
                    if (countToInfinity[coordX, coordY] > maxValue)
                    {
                        maxValue = countToInfinity[coordX, coordY];
                    }
                    else if (countToInfinity[coordX, coordY] < minValue)
                    {
                        if (countToInfinity[coordX, coordY] != 0)
                        {
                            minValue = countToInfinity[coordX, coordY];
                        }
                    }
                }
            }

            Console.WriteLine("Min : {0}", minValue);
            Console.WriteLine("Max : {0}", maxValue);
        }
    }

}
