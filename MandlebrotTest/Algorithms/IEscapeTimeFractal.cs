﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LinearAlgebra;
using ArbitraryPrecision;

namespace MandlebrotTest.Algorithms
{
    interface IEscapeTimeFractal
    {
        BigDecimalComplex Origin { get; set; }
        BigDecimalComplex C { get; set; }

        BigDecimalComplex[,] ComplexNumbers { get; }
        int[,] CountToInfinity { get; }
        int MinValue { get; }
        int MaxValue { get; }

        int Width { get; }
        int Height { get; }

        Boolean Initialized { get; }

        List<Preset> Presets { get; }

        void LoadPresets();
        void Generate(double zoom, int iterations, int width, int height);
        String ToString();
    }
}
