﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MandlebrotTest.Algorithms
{
    class NewtonFractal : IEscapeTimeFractal
    {
        /// See http://en.wikipedia.org/wiki/Newton_fractal [01-08-2014]

        public LinearAlgebra.Complex Origin
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public LinearAlgebra.Complex C
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public LinearAlgebra.Complex[,] ComplexNumbers
        {
            get { throw new NotImplementedException(); }
        }

        public int[,] CountToInfinity
        {
            get { throw new NotImplementedException(); }
        }

        public int MinValue
        {
            get { throw new NotImplementedException(); }
        }

        public int MaxValue
        {
            get { throw new NotImplementedException(); }
        }

        public int Width
        {
            get { throw new NotImplementedException(); }
        }

        public int Height
        {
            get { throw new NotImplementedException(); }
        }

        public bool Initialized
        {
            get { throw new NotImplementedException(); }
        }

        public List<Preset> Presets
        {
            get { throw new NotImplementedException(); }
        }

        public void LoadPresets()
        {
            throw new NotImplementedException();
        }

        public void Generate(double zoom, int iterations, int width, int height)
        {
            throw new NotImplementedException();
        }
    }
}
