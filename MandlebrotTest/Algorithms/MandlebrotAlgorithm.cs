﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LinearAlgebra;
using ArbitraryPrecision;
using System.IO;

namespace MandlebrotTest.Algorithms
{
    class MandlebrotAlgorithm : IEscapeTimeFractal
    {
        BigDecimalComplex[,] complexNo;
        int[,] countToInfinity;
        public BigDecimalComplex[,] ComplexNumbers { get { return complexNo; } }
        public int[,] CountToInfinity { get { return countToInfinity; } }

        BigDecimalComplex origin;
        BigDecimalComplex c;
        public BigDecimalComplex Origin { get { return origin; } set { origin = value; } }
        public BigDecimalComplex C { get { return c; } set { c = value; } }

        List<Preset> presets = new List<Preset>();
        public List<Preset> Presets { get { return presets; } }

        int width;
        public int Width { get { return width; } }
        int height;
        public int Height { get { return height; } }

        int minValue = 0;
        int maxValue = 0;

        Boolean initialized;
        public Boolean Initialized { get { return initialized; } }

        public MandlebrotAlgorithm()
        {
            initialized = false;

            LoadPresets();
        }

        public void LoadPresets()
        {
            presets.Add(new Preset("Mandlebrot", new Complex(0, 0), 1, new Complex(0, 0)));
            presets.Add(new Preset("Skeptor", new Complex(-1.36, 0.005), 16, new Complex(0, 0)));
            presets.Add(new Preset("Tendrils", new Complex(-0.0002, 0.7383), 64, new Complex(0, 0)));
            presets.Add(new Preset("Mini Mandlebrot", new Complex(-1.75, 0.0), 32, new Complex(0, 0)));
            presets.Add(new Preset("Misiurewicz point", new Complex(-0.1011, 0.9563), 64, new Complex(0, 0)));
        }

        /**
         *      Create a mandlebrot fractal
         **/
        public void Generate(double zoom, int maxIterations, int width, int height)
        {
            complexNo = new BigDecimalComplex[width, height];
            countToInfinity = new int[width, height];

            this.width = width;
            this.height = height;

            //  Start with a range of 2 in each direction from zero
            //  as the zoom increases, this decreses to give more detail
            double r = 2.0d / zoom;

            BigDecimal LOWER_LIMIT_X = origin.Real - r;
            BigDecimal UPPER_LIMIT_X = origin.Real + r;
            BigDecimal RANGE_X = (UPPER_LIMIT_X - LOWER_LIMIT_X);// / zoom;

            BigDecimal LOWER_LIMIT_Y = origin.Imag - r;
            BigDecimal UPPER_LIMIT_Y = origin.Imag + r;
            BigDecimal RANGE_Y = (UPPER_LIMIT_Y - LOWER_LIMIT_Y);// / zoom;

            BigDecimal step_x = (RANGE_X / (double)width);
            BigDecimal step_y = (RANGE_Y / (double)height);

            BigDecimal POS_INFINITY = (BigDecimal)(float.MaxValue);
            BigDecimal NEG_INFINITY = (BigDecimal)(float.MinValue);

            //BigDecimal.Precision = 3;
            //BigDecimal.AlwaysTruncate = true;

            int progress = 0;
            int target = width * height;
#if PARALLEL
            Parallel.For(0, width, new ParallelOptions { MaxDegreeOfParallelism = 8 }, coordX =>
#else
            for (int coordX = 0; coordX < width; coordX++)
#endif
            {
                BigDecimal x0 = LOWER_LIMIT_X + (coordX * step_x);

                for (int coordY = 0; coordY < height; coordY++)
                {
                    //if ((coordX % 50 == 0) && (coordY % 50 == 0))
                    //    Console.WriteLine("{0},{1}", coordX, coordY);

                    BigDecimal y0 = LOWER_LIMIT_Y + (coordY * step_y);

                    complexNo[coordX, coordY] = new BigDecimalComplex(x0, y0);

                    //  Difference between Mandlebrot and
                    //  Julia set is that in Julia set the
                    //  multiplication constant is specified
                    //  whereas in a Mandlebrot fractal it
                    //  is fixed at the origin (z)
                    //BigDecimalComplex zN = new BigDecimalComplex(x0, y0);
                    //BigDecimalComplex c = zN;

                    BigDecimal x = 0;
                    BigDecimal y = 0;
                    
                    //int i;
                    int iterationWhereInfinite = 0;

                    while(((x*x) + (y*y) < (2*2)) && (iterationWhereInfinite < maxIterations))
                    {
                        BigDecimal xtemp = (x * x) - (y * y) + x0;
                        BigDecimal ytemp = (2 * x * y) + y0;
                        if ((x == xtemp) && (y == ytemp))
                        {
                            iterationWhereInfinite = maxIterations;
                            break;
                        }
                        x = xtemp;
                        y = ytemp;

                        //Console.WriteLine("{0}", iterationWhereInfinite);

                        ++iterationWhereInfinite;
                    }
                    /*
                    for (i = 0; i < iterations; i++)
                    {
                        zN = zN.Squared + c;
                        
                        if ((zN.Real >= POS_INFINITY) || (zN.Real <= NEG_INFINITY)
                            || (zN.Imag >= POS_INFINITY) || (zN.Imag <= NEG_INFINITY))
                        {
                            iterationWhereInfinte = i;
                            break;
                        }
                    }
                    */
                    //Console.WriteLine("{0}, {1}", coordX, coordY);


                    if (iterationWhereInfinite == maxIterations)
                        countToInfinity[coordX, coordY] = 0;
                    else
                        countToInfinity[coordX, coordY] = iterationWhereInfinite;
                    
                    //Console.WriteLine("{0},{1} = {2}", x, y, z.IsInfinite);
                    ++progress;

                    float percent = ((float)progress / (float)target) * 100.0f;

                    //if (percent % 2 == 0)
                        Console.WriteLine("{0}/{1} = {2}%", progress, target, percent);
                }
#if PARALLEL
                });
#else
            }
#endif
            dumpArray(countToInfinity, width, height);

            findMinAndMaxValues();

            initialized = true;
        }

        private void dumpArray(int[,] array, int sx, int sy)
        {
            StreamWriter sr = new StreamWriter("C:\\dump.txt");

            for (int y = 0; y < sy; ++y)
            {
                String s = "";

                for (int x = 0; x < sx; ++x)
                {
                    s += array[x, y].ToString();
                    s += "\t";
                }
                sr.WriteLine(s);
            }

            sr.Close();
        }

        public override string ToString()
        {
            return "Mandlebrot set";
        }

        private void findMinAndMaxValues()
        {
            maxValue = 0;
            minValue = 255;

            for (int coordX = 0; coordX < width; coordX++)
            {
                for (int coordY = 0; coordY < height; coordY++)
                {
                    if (countToInfinity[coordX, coordY] > maxValue)
                    {
                        maxValue = countToInfinity[coordX, coordY];
                    }
                    else if (countToInfinity[coordX, coordY] < minValue)
                    {
                        if (countToInfinity[coordX,coordY] != 0)
                        {
                            minValue = countToInfinity[coordX, coordY];
                        }
                    }
                }
            }

            Console.WriteLine("Min : {0}", minValue);
            Console.WriteLine("Max : {0}", maxValue);
        }

        public int MinValue
        {
            get { return minValue; }
        }

        public int MaxValue
        {
            get { return maxValue; }
        }
    }
}
